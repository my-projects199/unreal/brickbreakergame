// Fill out your copyright notice in the Description page of Project Settings.


#include "BrickCPP.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "BallCPP.h"

// Sets default values
ABrickCPP::ABrickCPP()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SM_Brick = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SM_Brick"));

	SM_Brick->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collision"));
	BoxCollision->SetBoxExtent(FVector(25.0f, 10.0f, 10.0f));

	RootComponent = BoxCollision;

}

// Called when the game starts or when spawned
void ABrickCPP::BeginPlay()
{
	Super::BeginPlay();

	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &ABrickCPP::OnOverlapComponent);
	
}

// Called every frame
void ABrickCPP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABrickCPP::OnOverlapComponent(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndexType, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor->ActorHasTag("Ball"))
	{
		ABallCPP* MyBall = Cast<ABallCPP>(OtherActor);

		FVector Velocity = MyBall->GetVelocity();
		Velocity *= (SpeedModifierOnBounce - 1.0f);

		MyBall->GetBall()->SetPhysicsLinearVelocity(Velocity, true);

		FTimerHandle UnusedHandle;
		GetWorldTimerManager().SetTimer(UnusedHandle, this, &ABrickCPP::DestroyBrick, 0.1f, false);
	}
}

void ABrickCPP::DestroyBrick()
{
	this->Destroy();
}