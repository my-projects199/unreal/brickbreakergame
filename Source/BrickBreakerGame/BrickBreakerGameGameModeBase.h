// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BrickBreakerGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BRICKBREAKERGAME_API ABrickBreakerGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
