// Copyright Epic Games, Inc. All Rights Reserved.

#include "BrickBreakerGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BrickBreakerGame, "BrickBreakerGame" );
